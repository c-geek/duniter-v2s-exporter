// #[allow(clippy::enum_variant_names)]
#[subxt::subxt(
runtime_metadata_path = "res/metadata.scale",
derive_for_all_types = "Debug"
)]
pub mod runtime {}

// declare custom types
pub type Client = subxt::OnlineClient<Runtime>;
pub type AccountId = subxt::utils::AccountId32;
pub type BlockNumber = u32;
pub type Hash = sp_core::H256;

// declare runtime types
pub enum Runtime {}
impl subxt::config::Config for Runtime {
    type Hash = Hash;
    type AccountId = AccountId;
    type Address = sp_runtime::MultiAddress<Self::AccountId, u32>;
    type Signature = sp_runtime::MultiSignature;
    type Hasher = subxt::config::substrate::BlakeTwo256;
    type Header = subxt::config::substrate::SubstrateHeader<BlockNumber, Self::Hasher>;
    type ExtrinsicParams = subxt::config::DefaultExtrinsicParams<Self>;
}
