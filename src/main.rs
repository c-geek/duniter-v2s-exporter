use std::sync::{Arc, Mutex};
use std::time::Duration;

use rocket::{launch, routes, tokio};
use rocket::tokio::time::sleep;

use crate::state::{Data, keep_up_state};

mod runtime;
mod state;
mod routes;

pub struct MyConfig {
    data: Arc<Mutex<Data>>,
    rt: tokio::runtime::Runtime,
}

#[launch]
async fn rocket() -> _ {
    let data = Arc::new(Mutex::new(Data::new()));

    let config = MyConfig {
        data: data.clone(),
        rt: tokio::runtime::Runtime::new().unwrap(),
    };

    config.rt.spawn(async move {
        loop {
            match keep_up_state(data.clone()).await {
                Ok(_) => {
                    eprintln!("Should not have finished?!");
                }
                Err(e) => {
                    eprintln!("Error: {}", e);
                }
            }
            println!("Sleeping for 10 seconds...");
            sleep(Duration::from_millis(10000)).await;
        }
    });

    rocket::build()
        .mount("/metrics", routes![crate::routes::metrics::route])
        .manage(config)
}
