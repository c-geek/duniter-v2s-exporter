use rocket::{get, State};
use crate::MyConfig;

#[get("/")]
pub fn route(state: &State<MyConfig>) -> String {
    let authorities_count = state.data.clone().lock().unwrap().authorities_count;
    let incoming_authorities = state.data.clone().lock().unwrap().incoming_authorities;
    let online_authorities = state.data.clone().lock().unwrap().online_authorities;
    let outgoing_authorities = state.data.clone().lock().unwrap().outgoing_authorities;
    let blacklisted_authorities = state.data.clone().lock().unwrap().blacklisted_authorities;
    let smiths_count = state.data.clone().lock().unwrap().smiths_count.unwrap();
    let smiths_variation = state.data.clone().lock().unwrap().smiths_variation;
    let validators_count = state.data.clone().lock().unwrap().validators_count.unwrap();
    let validators_variation = state.data.clone().lock().unwrap().validators_variation;
    format!(r#"# HELP gdev_authorities_count Authorities count
# TYPE gdev_authorities_count gauge
gdev_authorities_count{{chain="gdev_local"}} {}

# HELP gdev_authorities_online_count Authorities online count
# TYPE gdev_authorities_online_count gauge
gdev_authorities_online_count{{chain="gdev_local"}} {}

# HELP gdev_authorities_outgoing_count Authorities outgoing count
# TYPE gdev_authorities_outgoing_count gauge
gdev_authorities_outgoing_count{{chain="gdev_local"}} {}

# HELP gdev_authorities_incoming_count Authorities incoming count
# TYPE gdev_authorities_incoming_count gauge
gdev_authorities_incoming_count{{chain="gdev_local"}} {}

# HELP gdev_blacklisted_authorities_count Blacklisted authorities count
# TYPE gdev_blacklisted_authorities_count gauge
gdev_blacklisted_authorities_count{{chain="gdev_local"}} {}

# HELP gdev_smiths_count Smiths count
# TYPE gdev_smiths_count gauge
gdev_smiths_count{{chain="gdev_local"}} {}

# HELP gdev_smiths_variation Smiths variation
# TYPE gdev_smiths_variation gauge
gdev_smiths_variation{{chain="gdev_local"}} {}

# HELP gdev_validators_count Validators count
# TYPE gdev_validators_count gauge
gdev_validators_count{{chain="gdev_local"}} {}

# HELP gdev_validators_variation Validators variation
# TYPE gdev_validators_variation gauge
gdev_validators_variation{{chain="gdev_local"}} {}
"#,
            authorities_count,
            online_authorities,
            outgoing_authorities,
            incoming_authorities,
            blacklisted_authorities,
            smiths_count,
            smiths_variation,
            validators_count,
            validators_variation,
    ).into()
}