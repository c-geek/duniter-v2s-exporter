use std::sync::{Arc, Mutex};
use std::time::Duration;

use rocket::tokio::time::sleep;
use sp_core::H256;
use subxt::blocks::BlockRef;

use crate::runtime::{Client, Hash};
use crate::runtime;

const SLOT_DURATION_SECONDS: u64 = 6;
const BLOCKS_PER_MINUTE: u64 = 60 / SLOT_DURATION_SECONDS;
const BLOCKS_TO_KEEP: usize = BLOCKS_PER_MINUTE as usize;

#[derive(Clone, Debug)]
pub struct Data {
    pub authorities_count: u8,
    pub online_authorities: u8,
    pub incoming_authorities: u8,
    pub outgoing_authorities: u8,
    pub blacklisted_authorities: u8,
    pub smiths_count: Option<u32>,
    pub smiths_variation: i32,
    pub validators_count: Option<u32>,
    pub validators_variation: i32,
    pub previous_metrics: Vec<Data>,
    pub last_received: Option<Blockstamp>
}

impl Data {
    pub fn new() -> Self {
        Data {
            authorities_count: 0,
            online_authorities: 0,
            incoming_authorities: 0,
            outgoing_authorities: 0,
            blacklisted_authorities: 0,
            smiths_count: None,
            smiths_variation: 0,
            validators_count: None,
            validators_variation: 0,
            previous_metrics: vec![],
            last_received: None,
        }
    }
}

#[derive(Clone, Debug)]
pub struct Blockstamp {
    pub hash: H256,
    pub number: u32,
}

pub async fn keep_up_state(data: Arc<Mutex<Data>>) -> anyhow::Result<()> {
    loop {
        let client = &connect().await?;
        println!("Connected to gdev");
        let current_hash = client.backend().latest_finalized_block_ref().await?;
        let current_header = client.backend().block_header(current_hash.hash()).await?.unwrap();
        let last_received = data.lock().unwrap().last_received.clone();
        let is_different_number = last_received.is_none() || last_received.unwrap().number != current_header.number;
        if is_different_number {
            println!("block: {:?} {:?}", current_header.number, current_hash.hash());
            let new_state = get_new_state(client, current_header.number, current_hash).await.unwrap_or_else(|e| {
                println!("Error: {:?}", e);
                Data::new()
            });
            set_new_state(data.clone(), new_state).unwrap_or_else(|e| {
                println!("Error: {:?}", e);
            });
        }
        sleep(Duration::from_millis(3000)).await;
    }
}

fn set_new_state(data: Arc<Mutex<Data>>, new_state: Data) -> anyhow::Result<()> {
    let mut to_push = new_state.clone();
    data.lock().unwrap().authorities_count = new_state.authorities_count.clone();
    data.lock().unwrap().last_received = new_state.last_received.clone();
    data.lock().unwrap().online_authorities = new_state.online_authorities.clone();
    data.lock().unwrap().incoming_authorities = new_state.incoming_authorities.clone();
    data.lock().unwrap().outgoing_authorities = new_state.outgoing_authorities.clone();
    data.lock().unwrap().blacklisted_authorities = new_state.blacklisted_authorities.clone();
    let (smiths_variation, had_smith_variation) = set_smith_count_and_variation(data.clone(), &new_state, &mut to_push);
    let (validators_variation, had_validators_variation) = set_validators_count_and_variation(data.clone(), &new_state, &mut to_push);

    data.lock().unwrap().previous_metrics.push(to_push);
    if data.lock().unwrap().previous_metrics.len() > BLOCKS_TO_KEEP {
        data.lock().unwrap().previous_metrics.remove(0);
    }
    let v = data.lock().unwrap().previous_metrics.iter().map(|x| x.smiths_variation).collect::<Vec<i32>>();
    println!("smiths_variation: {}, had_previous: {}, v: {:?}", smiths_variation, had_smith_variation, v);
    println!("validators_variation: {}, had_previous: {}, v: {:?}", validators_variation, had_validators_variation, v);

    Ok(())
}

fn set_smith_count_and_variation(data: Arc<Mutex<Data>>, new_state: &Data, to_push: &mut Data) -> (i32, bool) {
    let last_smiths_count = data.lock().unwrap().smiths_count;
    let local_smith_variation = if last_smiths_count.is_some() {
        new_state.smiths_count.unwrap() as i32 - last_smiths_count.unwrap() as i32
    } else {
        0
    };
    data.lock().unwrap().smiths_count = new_state.smiths_count;
    let mut had_smith_variation = local_smith_variation != 0;
    for previous_metric in data.lock().unwrap().previous_metrics.iter() {
        had_smith_variation = had_smith_variation || previous_metric.smiths_variation != 0;
    }
    let smiths_variation = if had_smith_variation { 1 } else { 0 };
    data.lock().unwrap().smiths_variation = smiths_variation;
    to_push.smiths_variation = local_smith_variation;
    (smiths_variation, had_smith_variation)
}

fn set_validators_count_and_variation(data: Arc<Mutex<Data>>, new_state: &Data, to_push: &mut Data) -> (i32, bool) {
    let last_validators_count = data.lock().unwrap().validators_count;
    let local_validators_variation = if last_validators_count.is_some() {
        new_state.validators_count.unwrap() as i32 - last_validators_count.unwrap() as i32
    } else {
        0
    };
    data.lock().unwrap().validators_count = new_state.validators_count;
    let mut had_validators_variation = local_validators_variation != 0;
    for previous_metric in data.lock().unwrap().previous_metrics.iter() {
        had_validators_variation = had_validators_variation || previous_metric.validators_variation != 0;
    }
    let validators_variation = if had_validators_variation { 1 } else { 0 };
    data.lock().unwrap().validators_variation = validators_variation;
    to_push.validators_variation = local_validators_variation;
    (validators_variation, had_validators_variation)
}

async fn get_new_state(client: &Client, current_number: u32, current_hash: BlockRef<Hash>) -> anyhow::Result<Data> {
    let x = runtime::runtime::storage().authority_members().members_iter();
    let mut authority_iter = client
        .storage()
        .at(current_hash.clone())
        .iter(x)
        .await?;
    let mut authorities_count = 0;
    while let Some(Ok(_)) = authority_iter.next().await {
        authorities_count += 1;
    }
    let incoming_authorities = client
        .storage()
        .at(current_hash.clone())
        .fetch(&runtime::runtime::storage().authority_members().incoming_authorities())
        .await?
        .unwrap_or_else(|| vec![])
        .len() as u8;
    let online_authorities = client
        .storage()
        .at(current_hash.clone())
        .fetch(&runtime::runtime::storage().authority_members().online_authorities())
        .await?
        .unwrap_or_else(|| vec![])
        .len() as u8;
    let outgoing_authorities = client
        .storage()
        .at(current_hash.clone())
        .fetch(&runtime::runtime::storage().authority_members().outgoing_authorities())
        .await?
        .unwrap_or_else(|| vec![])
        .len() as u8;
    let blacklisted_authorities = client
        .storage()
        .at(current_hash.clone())
        .fetch(&runtime::runtime::storage().authority_members().blacklist())
        .await?
        .unwrap_or_else(|| vec![])
        .len() as u8;
    let smiths_storage_query = runtime::runtime::storage().smith_members().smiths_iter();
    let mut smiths = client
        .storage()
        .at(current_hash.hash())
        .iter(smiths_storage_query)
        .await?;
    let mut smiths_count = 0;
    while smiths.next().await.is_some() {
        smiths_count += 1;
    }
    let validators_count = client
        .storage()
        .at(current_hash.clone())
        .fetch(&runtime::runtime::storage().session().validators())
        .await?
        .unwrap_or_else(|| vec![])
        .len() as u32;
    Ok(Data {
        authorities_count,
        online_authorities,
        incoming_authorities,
        outgoing_authorities,
        blacklisted_authorities,
        smiths_count: Some(smiths_count),
        smiths_variation: 0,
        validators_count: Some(validators_count),
        validators_variation: 0,
        previous_metrics: vec![],
        last_received: Some(Blockstamp {
            hash: current_hash.hash(),
            number: current_number,
        }),
    })
}

async fn connect() -> Result<Client, anyhow::Error> {
    let rpc_url = std::env::var("RPC_URL").unwrap_or_else(|_| "wss://gdev.cgeek.fr/ws".to_string());
    let client = Client::from_url(rpc_url);
    let client = client.await?;
    Ok(client)
}
#[cfg(test)]
mod test {
    use std::sync::{Arc, Mutex};

    use crate::state::{Data, set_new_state};

    #[test]
    fn test() {
        let global_state: Arc<Mutex<Data>> = Arc::new(Mutex::new(Data::new()));

        // Pas de variation au premier appel
        set_new_state(global_state.clone(), new_state(2, 2)).unwrap();
        assert_eq!(global_state.lock().unwrap().smiths_count, Some(2));
        assert_eq!(global_state.lock().unwrap().smiths_variation, 0);
        assert_eq!(global_state.lock().unwrap().validators_count, Some(2));
        assert_eq!(global_state.lock().unwrap().validators_variation, 0);

        // Mais variation au second
        set_new_state(global_state.clone(), new_state(3, 3)).unwrap();
        assert_eq!(global_state.lock().unwrap().smiths_count, Some(3));
        assert_eq!(global_state.lock().unwrap().smiths_variation, 1);
        assert_eq!(global_state.lock().unwrap().validators_count, Some(3));
        assert_eq!(global_state.lock().unwrap().validators_variation, 1);
        // Qui doit persister au 3ème
        set_new_state(global_state.clone(), new_state(3, 3)).unwrap();
        assert_eq!(global_state.lock().unwrap().smiths_count, Some(3));
        assert_eq!(global_state.lock().unwrap().smiths_variation, 1);
        assert_eq!(global_state.lock().unwrap().validators_count, Some(3));
        assert_eq!(global_state.lock().unwrap().validators_variation, 1);
        // Malgré la redescente, au global il y a bien eu une variation
        set_new_state(global_state.clone(), new_state(2, 2)).unwrap();
        assert_eq!(global_state.lock().unwrap().smiths_count, Some(2));
        assert_eq!(global_state.lock().unwrap().smiths_variation, 1);
        assert_eq!(global_state.lock().unwrap().validators_count, Some(2));
        assert_eq!(global_state.lock().unwrap().validators_variation, 1);
    }

    fn new_state(smiths: u32, validators: u32) -> Data {
        let mut new_state = Data::new();
        new_state.smiths_count = Some(smiths);
        new_state.validators_count = Some(validators);
        new_state
    }
}