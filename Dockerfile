FROM rust:1-bullseye as build

COPY . .

# ------------------------------------------------------------------------------
# Install dependencies
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y clang cmake protobuf-compiler

# ------------------------------------------------------------------------------ \
RUN cargo build --release
RUN mv target/release/duniter-v2s-exporter /usr/local/bin/duniter-v2s-exporter
RUN cargo clean

FROM debian:bullseye-slim

RUN apt-get update \
 && apt-get install -y --no-install-recommends ca-certificates

RUN update-ca-certificates

COPY --from=build /usr/local/bin/duniter-v2s-exporter /usr/local/bin/duniter-v2s-exporter

# ------------------------------------------------------------------------------
CMD ["duniter-v2s-exporter"]